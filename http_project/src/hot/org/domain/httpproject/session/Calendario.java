package org.domain.httpproject.session;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.context.FacesContext;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;

@Name("Calendario")
public class Calendario {

	@In
	FacesMessages facesMessages;

	private Date date;

	public void send() {

		if (date == null) {
			facesMessages.add("Por favor, selecione uma data");
			return;
		}
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.setTime(date);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = df.format(date);
		String year = String.valueOf(calendar.get(java.util.Calendar.YEAR));
		String month = String.format("%02d", calendar.get(java.util.Calendar.MONTH) + 1);
		String day = String.format("%02d", calendar.get(java.util.Calendar.DATE));

		String url = "http://example.com/";
		url = url + year + "/" + month + "/" + day + "?data=" + formattedDate;

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {
			facesMessages.add(e.getMessage());
		}
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
